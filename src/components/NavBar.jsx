import React from 'react';
import {
    makeStyles,
    ThemeProvider,
    createMuiTheme
} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { purple } from '@material-ui/core/colors';
import Grid from '@material-ui/core/Grid';
import { IconButton } from '@material-ui/core';
import AccountCircleTwoToneIcon from '@material-ui/icons/AccountCircleTwoTone';
import ExitToAppTwoToneIcon from '@material-ui/icons/ExitToAppTwoTone';
import NotificationsTwoToneIcon from '@material-ui/icons/NotificationsTwoTone';

const useStyles = makeStyles({
});

const theme = createMuiTheme({
    palette: {
        primary: {
            main: purple[500]
        }
    }
})

export default function CenteredTabs() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <Paper>
            <Grid container className={classes.display}>
                <ThemeProvider theme={theme}>
                    <Grid item xs={10}>
                        <Tabs
                            value={value}
                            onChange={handleChange}
                            indicatorColor="primary"
                            textColor="primary"
                        >
                            <Tab label="Missões" />
                            <Tab label="Inventário" />
                            <Tab label="Amigos" />
                            <Tab label="Loja" />
                        </Tabs>
                    </Grid>
                    <Grid item xs={2}
                        container
                        direction="row"
                        justify="flex-end"
                        alignItems="flex-start">
                        <IconButton aria-label="Notificação">
                            <NotificationsTwoToneIcon color="primary" />
                        </IconButton>
                        <IconButton aria-label="Perfil">
                            <AccountCircleTwoToneIcon color="primary" />
                        </IconButton>
                        <IconButton aria-label="Sair">
                            <ExitToAppTwoToneIcon color="primary" />
                        </IconButton>
                    </Grid>
                </ThemeProvider >
            </Grid>
        </Paper>
    );
}
