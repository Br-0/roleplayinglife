import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import NavBar from './components/NavBar'

function App() {
  return (
    <div>
      <Router>
        <Switch>
          <Route path="/">
            <NavBar />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
